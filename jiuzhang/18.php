<?php 
//操作给定的二叉树，将其变换为源二叉树的镜像。
/*class TreeNode{
    var $val;
    var $left = NULL;
    var $right = NULL;
    function __construct($val){
        $this->val = $val;
    }
}*/
function Mirror($root)
{
    if($root==NULL) return;
    $node = $root->left;
    $root->left = $root->right;
    $root->right = $node;
    Mirror($root->left);
    Mirror($root->right);
}
 ?>