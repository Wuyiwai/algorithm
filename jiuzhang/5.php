<?php 
	//用两个栈来实现一个队列，完成队列的Push和Pop操作。 队列中的元素为int类型。
	//array_pop()返回pop的数据
	$arr1 = array();
	$arr2 = array();

	function mypush($node)
	{
	    global $arr1;
	    global $arr2;

	    array_push($arr1, $node);
	}
	function mypop()
	{
	    global $arr1;
	    global $arr2;
	    if(!empty($arr2))
	    {
	    	return array_pop($arr2);
	    }
	    else
	    {
	    	while(!empty($arr1))
	    	{
	    		array_push($arr2, array_pop($arr1));
	    	}
	    	return array_pop($arr2);
	    }
	}
 ?>