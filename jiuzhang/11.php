<?php 
	//输入一个整数，输出该数二进制表示中1的个数。其中负数用补码表示。
	function NumberOf1($n)
	{
		if($n==0) return 0;
		$sum = 0;
		$flag = 1;
		while($flag)
		{
			if($n&$flag)
			{
				$sum ++;
			}
			$flag = $flag << 1;
		}
		return $sum;
	}
 ?>