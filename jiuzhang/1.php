<?php 
	// 在一个二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
	function Find($target, $array)
	{
		$row = count($array);
	   	$col = count($array[0]);
	   	if($row==0) return false;
	   	if($col==0) return false;
	   	for($i=0;$i<$row;$i++)
	   	{
	   		for($j=0;$j<$col;$j++)
	   		{
	   			if($target == $array[$i][$j])
	   			{
	   				return true;
	   			}
	   			else
	   			{
	   				continue;
	   			}
	   		}
	   	}
	}

	// $target = 7;
	// $array = array(
	// 	array(1,2,7,9),
	// 	array(2,4,9,12),
	// 	array(4,7,10,13),
	// 	array(6,8,11,15)
	// );
	// Find($target, $array);
 ?>