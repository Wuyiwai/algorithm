<?php 
	// 大家都知道斐波那契数列，现在要求输入一个整数n，请你输出斐波那契数列的第n项。n<=39
	function Fibonacci($n)
	{
		// 1 1 2 3 5 8 13 21
		if($n==0) return 0;
		$tmp = 1;
		$ret = 0;
		for($i=1;$i<=$n;$i++)
		{
			$ret += $tmp;
			$tmp = $ret - $tmp;
		}
		return $ret;
	}
 ?>