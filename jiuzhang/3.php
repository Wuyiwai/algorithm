<?php 
	//输入一个链表，从尾到头打印链表每个节点的值。
	/*class ListNode{
	    var $val;
	    var $next = NULL;
	    function __construct($x){
	        $this->val = $x;
	    }
	}*/

	//递归
	function printListFromTailToHead($head)
	{
		if($head == null) return [];
		$arr = array();
		$cur = $head;//头指针
	    if($cur->next != null)
	    {
	    	$arr = printListFromTailToHead($cur->next);
	    }
	    array_push($arr,$cur->val);
	    return $arr;
	}

	//非递归
	function printListFromTailToHead($head)
	{
		if($head == null) return [];
		$arr = array();
		$cur = $head;//头指针
		$re = array();
	    
	    while($cur != null)
	    {
	    	array_push($arr, $cur->val);
	    	$cur = $cur->next;
	    }

	    while(!empty($arr))
	    {
	    	$tmp = array_pop($arr);
	    	array_push($re,$tmp);
	    }//从尾部到头部打印。
	    return $re;
	}
 ?>