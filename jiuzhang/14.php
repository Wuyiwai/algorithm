<?php 
//输入一个链表，输出该链表中倒数第k个结点。
/*class ListNode{
    var $val;
    var $next = NULL;
    function __construct($x){
        $this->val = $x;
    }
}*/
function FindKthToTail($head, $k)
{
    if($head == NULL || $k ==0)
        return  NULL;

    $pre = $head;
    $last = $head;

    for($i=1; $i<$k; $i++){
        if($last->next == NULL)
            return NULL;
        else
            $last = $last->next;
    }
    while($last->next != NULL){
        $pre = $pre->next;
        $last = $last->next;
    }

    return $pre;
}
 ?>