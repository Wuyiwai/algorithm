<?php 
//输入一个链表，反转链表后，输出链表的所有元素。
//解题思路，通过递归，递归到最后一个节点。再翻转设置指针。
/*class ListNode{
var $val;
var $next = NULL;
function __construct($x){
    $this->val = $x;
}
}*/
function ReverseList($pHead)
{
    if($pHead==NULL) return NULL;
    if($pHead->next==NULL) return $pHead;
    $node = $pHead->next;
    $tmpHead = ReverseList($node);
    $node->next = $pHead;
    $pHead->next = NULL;
    return $tmpHead;
}
 ?>