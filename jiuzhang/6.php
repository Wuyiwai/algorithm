<?php 
	/*把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。输入一个非递减排序的数组的一个旋转，输出旋转数组的最小元素。 例如数组{3,4,5,1,2}为{1,2,3,4,5}的一个旋转，该数组的最小值为1。 NOTE：给出的所有元素都大于0，若数组大小为0，请返回0。*/
	function minNumberInRotateArray($rotateArray)
	{
	    $length = count($rotateArray);
	    if($length==0) return 0;
	    $low = 0;
	    $high = $length-1;
	    while ($low < $high) 
	    {
	    	if($rotateArray[$low]<$rotateArray[$high]) break;
	    	else
	    	{
	    		$mid = ($low+$high)/2;
	    		if($rotateArray[$mid]>$rotateArray[$low]) $low = $mid;
	    		else if($rotateArray[$mid]<$rotateArray[$mid-1])
	    		{
	    			$low = $mid;
	    			break;
	    		}
	    		else $high = $mid;
	    	}
	    }
	    return $rotateArray[$low];
	}
	//解决关键在于翻转数组，数组本身有序，所以把前面一部分翻转到尾部之后，二分查找就要改变一下。每次都要吧mid 和 mid-1作对比。
 ?>