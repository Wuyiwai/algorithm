<?php 
	// 输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前半部分，所有的偶数位于位于数组的后半部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变。
	function reOrderArray($array)
	{
		$length = count($array);
		for($i=0;$i<$length;$i++)
		{
			if(($array[$i] & 0x1))
			{
				continue;
			}
			$j = $i+1;
			while($j<$length)
			{
				if(($array[$j]&0x1))
				{
					while($j>$i)
					{
						$tmp = $array[$j];
						$array[$j] = $array[$j-1];
						$array[$j-1] = $tmp;
						--$j;
					}
					break;
				}
				++$j;
			}
		}
		return $array;
	}
 ?>