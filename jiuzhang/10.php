<?php 
	//递推公式 f(n)=f(n-1)+f(n-2) ，f(1)=1,f(2)=2
	function rectCover($number)
	{
		if($number<=0) return 0;
		$tmp = 1;
		$ret = 1;
		for($i=2;$i<=$number;$i++)
		{
			$ret += $tmp;
			$tmp = $ret-$tmp;
		}
		return $ret;
	}
 ?>