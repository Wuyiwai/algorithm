<?php 
	//给定一个double类型的浮点数base和int类型的整数exponent。求base的exponent次方。
	function Power($base, $exponent)
	{
	    if($exponent < 0) {
	        if($base === 0) {
	            return false;
	        }else {
	            if(-$exponent % 2 == 1) { 
	                return 1/(Power($base,-$exponent-1) * $base); 
	            }else { 
	                $r = 1/Power($base,-$exponent/2); 
	            return $r * $r; 
	            }
	        }
	    }
	    if($exponent === 0) { 
	        return 1; 
	    } 
	    else {
	        if($exponent % 2 == 1) { 
	            return Power($base,$exponent-1) * $base; 
	        }else { 
	            $res = Power($base,$exponent/2); 
	            return $res * $res; 
	        }
	    }
	}
 ?>