# Link: https://leetcode-cn.com/problems/reverse-string/submissions/
class Solution:
    def reverseString(self, s):
        """
        :type s: str
        :rtype: str
        """
        return s[::-1]