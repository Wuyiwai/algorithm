# Link
# En: https://leetcode.com/problems/sort-array-by-parity-ii/
# 中: https://leetcode-cn.com/problems/sort-array-by-parity-ii/

class Solution:
    def sortArrayByParityII(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        res = [0] * len(A)
        i,j = 0,1

        for elem in A:
            if elem % 2 == 0:
                res[i] = elem
                i += 2
            else:
                res[j] = elem
                j += 2

        return res