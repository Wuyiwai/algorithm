# Link: https://leetcode.com/problems/jewels-and-stones
class Solution:
    def numJewelsInStones(self, J, S):
        count = 0
        for c in S:
            for i in J:
                if c == i:
                    count+=1
                    break
        return count