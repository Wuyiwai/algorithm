# Link: https://leetcode.com/problems/n-repeated-element-in-size-2n-array/
from collections import Counter
class Solution:
    def repeatedNTimes(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        count = Counter(A)
        for key in count:
            if count[key] > 1:
                return key

