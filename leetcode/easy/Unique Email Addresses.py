# Link: https://leetcode.com/problems/unique-email-addresses/
class Solution:
    def numUniqueEmails(self, emails):
        """
        :type emails: List[str]
        :rtype: int
        """
        result = set()
        for email in emails:
            local, ex, domain = email.partition('@')
            if '+' in local:
                # 截取到+
                local = local[:local.index('+')]
            result.add(local.replace('.','') + '@' + domain)
        return len(result)

