# Link: https://leetcode-cn.com/problems/fibonacci-number/
class Solution:
    def fib(self, N):
        """
        :type N: int
        :rtype: int
        """
        if N < 2:
            return N
        else:
            return self.fib(N-1) + self.fib(N-2)