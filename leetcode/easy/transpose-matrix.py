# Link: https://leetcode-cn.com/problems/transpose-matrix/
class Solution:
    def transpose(self, A):
        """
        :type A: List[List[int]]
        :rtype: List[List[int]]
        """
        R, C = len(A), len(A[0])
        result = [[None] * R for _ in range(C)]

        for index,item in enumerate(A):
            for i,data in enumerate(item):
                result[i][index] = data
        return result
