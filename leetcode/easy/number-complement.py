# Link
# En: https://leetcode.com/problems/number-complement/
# 中: https://leetcode-cn.com/problems/number-complement/

class Solution:
    def findComplement(self, num):
        """
        :type num: int
        :rtype: int
        """
        b_code = bin(num)
        return int(b_code[:2] + b_code[2:].replace('0', '3').replace('1', '0').replace('3', '1'), 2)

'''
1. int(x, 2) 二进制转x十进制
2. bin(num) 返回整数的二进制表示
'''