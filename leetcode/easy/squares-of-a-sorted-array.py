# Link: https://leetcode-cn.com/problems/squares-of-a-sorted-array/

class Solution:
    def sortedSquares(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        result = []
        for i in A:
            result.append(i**2)
        result.sort()
        return result