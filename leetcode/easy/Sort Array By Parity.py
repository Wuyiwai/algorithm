# Link: https://leetcode.com/problems/sort-array-by-parity/
class Solution:
    def sortArrayByParity(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
