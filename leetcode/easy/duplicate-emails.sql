# Link
# En: https://leetcode.com/problems/duplicate-emails/
# 中: https://leetcode-cn.com/problems/duplicate-emails/

select Email from Person group by Email having count(Email)>1;