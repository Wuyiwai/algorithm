# Link: https://leetcode.com/problems/to-lower-case/
class Solution:
    def toLowerCase(self, str):
        result = ""
        for i in str:
            c = ord(i)
            if c>64 and c<91:
                result += chr(c + 32)
            else:
                result += i
        return result