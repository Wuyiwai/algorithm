# Link: https://leetcode.com/problems/flipping-an-image/
class Solution:
    def flipAndInvertImage(self, A):
        for item in A:
            item.reverse()
            for i in range(len(item)):
                if item[i] == 0:
                    item[i] = 1
                else:
                    item[i] = 0
        return A

# other solution
# def flipAndInvertImage(self, A):
#     res = []
#     for i in A:
#         res.append([0 if x == 1 else 1 for x in i][::-1])
#     return res