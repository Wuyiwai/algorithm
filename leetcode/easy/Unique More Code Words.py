# Link: https://leetcode.com/problems/unique-morse-code-words/
class Solution:
    def uniqueMorseRepresentations(self, words):
        mossList = [
            ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..",
            ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.",
            "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."
        ]
        result_list = set()
        for item in words:
            temp_str = ""
            for i in item:
                temp_str += mossList[ord(i)-97]
            result_list.add(temp_str)
        return result_list.__len__()